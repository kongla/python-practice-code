# 方向
此题第一反应是通过二分法求得需要金币的最小值，但是当我们例举情况时就会发现还是需要枚举猜的数来最终确定答案

n = 2 3 4 5 6 7 8 9

ans = 1 2 4 6 8 10 12 14

既然是枚举所有可能猜到的数，那多半是深度优先搜索&动态规划问题
# 思路
## 状态定义
dp[i][j]，i为左边界，j为右边界。答案即为dp[1][n]

## 转移方程

假设k为我们猜测的数，那么当我们要猜1-n之间的答案时，为了达到必胜条件所以猜测的k必然需要猜错，此时需要的金币为k。答案肯定在k左右区间其中一边，为了保证比胜那么我们应该再取其中需要金币多的那边。那么此时我们要求的dp[i][j] = max(k的左边区间需要的最少金币, k的右边区间需要的最少金币) + k,因而我们只需要枚举区间i-j之间所有k的情况求最小值便是区间i-j的答案

因为i为左边界，j为右边界。所以始终需要保持j>=i，所以j的遍历方向为正方向。如果我们i的方向也为正方向遍历，而此时我们的子状态(k的左右区间金币)还是未求出的，不符合动态规划的定义。所以i的遍历方向需要逆向遍历，确保每个状态由计算过的子状态得来
# 代码
```python
class Solution:
    def getMoneyAmount(self, n: int) -> int:
        # 当i <= j,即左边界小于等于右边界时答案为0，不妨都初始化为0
        dp = [[0] * (n + 1) for _ in range(n + 1)]
        # 枚举左边界i
        for i in range(n - 1, 0, -1):
            # 基于左边界i枚举右边界j
            for j in range(i + 1, n + 1):
                minn = float('inf')
                # 枚举猜测的数k，有i<=k<=j
                # 这里k可以不取到j，因为当k从小到大遍历到k == j - 1时
                # 只剩下两个数我们没有猜过了，肯定会猜小的那一个数来使金币用的最少
                # 另一个最大的数就不用猜了
                for k in range(i, j):
                    left = dp[i][k - 1] 
                    right = dp[k + 1][j]
                    minn = min(minn, k + max(left, right))
                dp[i][j] = minn
        return dp[1][n]
```
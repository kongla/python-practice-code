class Solution:
    def findPoisonedDuration(self, timeSeries: List[int], duration: int) -> int:
        n = len(timeSeries)
        if n == 0:
            return 0
        
        res = 0
        T = timeSeries[0]
        for i in range(1, n):
            if T + duration < timeSeries[i]:
                res += duration
                T = timeSeries[i]
            else:
                res += timeSeries[i] - T
                T = timeSeries[i]
        return res + duration
